const mongoose = require('mongoose');

const uri = "mongodb+srv://test:test@client-imfh5.gcp.mongodb.net/test?retryWrites=true&w=majority";

mongoose.Promise = global.Promise;
mongoose.connect(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    serverSelectionTimeoutMS: 5000
  })
  .then(()=> console.log("****** Mongo started"))
  .catch(err => console.log("Mongo - erro when started \n\n", err.reason))

const userSchema = new mongoose.Schema({
    username: {
        type: String,
         required: true
    },
    email: String
    },
    { collection: 'qualicorp'}
)

module.exports = {Mongoose : mongoose, UserSchema : userSchema};
