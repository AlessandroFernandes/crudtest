import React from 'react';

import clientService from './service/clientService';

import Container from '@material-ui/core/Container';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import MaterialTable from 'material-table';

export default function App() {
  const [state, setState] = React.useState({
    columns: [
      { title: 'username', field: 'username' },
      { title: 'email', field: 'email' },
    ],
  });
  const [userData, SetUserData] = React.useState([]);

  React.useEffect(() => { getClients()},[]);

  const getClients = () => {
    clientService.getClient()
                 .then(data => data.json())
                 .then(data => SetUserData(data))
                 .catch(err => console.error('Erro fetching customers', err));
  }

  const postClient = client => {
    clientService.postClient(client)
                 .then(()=> console.log('Success in the process'))
                 .catch(erro => console.error('Error adding customer', erro))
  };

  return (
    <Container>
      <AppBar position="static"></AppBar>
      <Toolbar variant="dense"></Toolbar>
      <MaterialTable
        title="Editable Example"
        columns={state.columns}
        data={userData}
        editable={{
          onRowAdd: (newData) =>
            new Promise((resolve) => {
              setTimeout(() => {
                resolve();
                postClient(newData);
                const dataCopy = [...userData, newData]; 
                SetUserData(dataCopy);
              }, 600);
            }),
          onRowUpdate: (newData, oldData) =>
            new Promise((resolve) => {
              setTimeout(() => {
                resolve();
                if (oldData) {
                  const updateUser = [...userData ];
                    let dataData  = updateUser[updateUser.indexOf(oldData)];
                    updateUser[updateUser.indexOf(oldData)] = newData;
                    clientService.updateClient(dataData, newData);
                    SetUserData(updateUser);
                }
              }, 600);
            }),
          onRowDelete: (oldData) =>
            new Promise((resolve) => {
              setTimeout(() => {
                resolve();
                const listUser = [...userData];
                listUser.splice(listUser.indexOf(oldData), 1);
                clientService.deleteClient(oldData);
                SetUserData(listUser);
              }, 600);
            }),
        }}
      />
    </Container>
  );
}

