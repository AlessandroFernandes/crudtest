module.exports = app => {

    app.get('/client', (req, res) => {
        const db = require('../data/db');
        const users = db.Mongoose.model("client", db.UserSchema);
        users
            .find({}, (err, data) => {
                res.status(200).send(data)
            })
    });

    app.post('/client', (req, res, next) => {
        const db = require('../data/db');
        const users = db.Mongoose.model("client", db.UserSchema);
        const user =  new users(req.body);
        user.save((erro) => {
            if(erro) console.log(erro);
            res.status(201).send(req.body)
        })
    });

   app.post('/client/delete', (req, res, next) => {
    const db = require('../data/db');
    const users = db.Mongoose.model("client", db.UserSchema);
    users
        .deleteOne({username: req.body.username, email: req.body.email}, erro => {
            if(erro) return console.log(erro);
            res.status(204).send(console.log('Successfully deleting'))
        })
   });

   app.post('/client/edit/:username', (req, res, next) => {
    const db = -require('../data/db');
    const users = db.Mongoose.model("client", db.UserSchema);

    const client = req.params;
    users
        .updateOne(client, req.body, erro => {
            if(erro) return console.log(erro);
            res.status(202).send(console.log('Successfully updating'))
        })
   })

}