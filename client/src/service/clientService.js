const URL = 'http://localhost:5000';

export default class ClientService {

    static getClient() {
        return fetch(`${URL}/client`);
    };

    static postClient(client) {
        return fetch(`${URL}/client`, {
                            method: 'post',
                            body: JSON.stringify(client),
                            headers: {
                                "Content-type": "application/json"
                            }
                        });
    };

    static deleteClient(user) {
        return fetch(`${URL}/client/delete`, {
                        method: 'post',
                        body: JSON.stringify(user),
                        headers: {
                            "Content-type": "application/json"
                        }
                     });
    }

    static updateClient(username, client) {
        return fetch(`${URL}/client/edit/${username.username}`,  {
                        method: 'post',
                        body: JSON.stringify(client),
                        headers: {
                            "Content-type": "application/json"
                    }
                });
    };
}